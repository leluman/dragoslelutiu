package week13homework.domain;

import java.sql.Connection;
import java.sql.Statement;

public class Create_Tables {
    public static void main(String[] args) {

        Connection connection = null;

        Statement statement = null;

        ConnectDB obj_ConnectDB = new ConnectDB();

        connection = obj_ConnectDB.get_Connection();

        try {

            String query="CREATE TABLE accommodation (\n" +
                    "    id SERIAL PRIMARY KEY,\n" +
                    "    type varchar(32),\n" +
                    "    bed_type varchar(32),\n" +
                    "    max_guests int,\n" +
                    "    description varchar(512)\n" +
                    ");\n";


            statement = connection.createStatement();
            statement.executeUpdate(query);

            String query2="CREATE TABLE room_fair (\n" +
                    "    id int PRIMARY KEY,\n" +
                    "    value double precision,\n" +
                    "    season varchar(32)\n" +
                    ")";

            statement = connection.createStatement();
            statement.executeUpdate(query2);

            String query3="CREATE TABLE accommodation_room_fair_relation (\n" +
                    "    id int PRIMARY KEY,\n" +
                    "    accommodation_id int,\n" +
                    "    room_fair_id int,\n" +
                    "\tFOREIGN KEY (accommodation_id)\n" +
                    "\t\tREFERENCES accommodation (id),\n" +
                    "\tFOREIGN KEY (room_fair_id)\n" +
                    "\t\tREFERENCES room_fair (id)\n" +
                    ")";

            statement = connection.createStatement();
            statement.executeUpdate(query3);

            System.out.println("finished!");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

