package week13homework.domain;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectDB {


    public Connection get_Connection() {

        Connection c = null;


        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/booking", "postgres", "superadmin");

            if (c != null) {
                System.out.println("Connection OK!");
            } else {
                System.out.println("Connection Failed");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return c;
    }
}
