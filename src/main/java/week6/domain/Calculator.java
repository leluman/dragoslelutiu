package week6.domain;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

    private String scale;

    public Calculator(String scale) {
        this.scale = scale;
    }

    public double calculate(String exp) {
        List<String> addList = new ArrayList<>();
        List<String> minusList = new ArrayList<>();
        int checkPoint = 0;
        boolean op = true;  //default first value is plus

        // Split string with plus/minus

        for (int i = 1; i < exp.length(); i++) {
            String s = exp.substring(i, i + 1);
            if (s.equals("*") || (s.equals("/"))) {
                throw new IllegalArgumentException("Invalid value");
            }
            if (Operator.PLUS.getOperator().equals(s)) {
                checkOperator(addList, minusList, op, exp.substring(checkPoint, i).trim());
                checkPoint = i + 1;
                op = true;
                continue;
            }
            if (Operator.MINUS.getOperator().equals(s)) {
                checkOperator(addList, minusList, op, exp.substring(checkPoint, i).trim());
                checkPoint = i + 1;
                op = false;
                continue;
            }
        }
        // Add last string
        checkOperator(addList, minusList, op, exp.substring(checkPoint).trim());
        // Get sum each list
        int sumAdd = sumList(addList);
        int sumMinus = sumList(minusList);

        int sum = sumAdd - sumMinus;
        String s = getScale();
        double scale = MetricConvertor.valueOf(s).getScale();
        return sum / scale;
    }
    //sum a list
    private static int sumList(List<String> addList) {
        int sum = 0;
        for (String s: addList) {
            String[] arr = s.split(" ");
            int value = Integer.parseInt(arr[0]);
            int scale = MetricConvertor.valueOf(arr[1]).getScale();
            sum += value * scale;
        }
        return sum;
    }
    // check operator to put into appropriate list
    private static void checkOperator(List<String> addList, List<String> minusList, boolean op, String substring) {
        if (op) {
            addList.add(substring);
        } else {
            minusList.add(substring);
        }
    }

    public String getScale() {
        return scale;
    }
}

