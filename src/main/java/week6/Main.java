import week6.domain.Calculator;
import week6.domain.MetricConvertor;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String scale, expression;

        System.out.println("Hello, this is a calculator that is capable of " +
                "adding and/or subtracting a metric distance value from an " +
                "\n expression that contains different scales and systems. \n");

        System.out.println("Example of expression to be input: 10 cm + 1 m - 10 mm \n");

        //USER INPUTS EXPRESSION

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please input the expression to be calculated: ");
        expression = scanner.nextLine();

        System.out.println("You can choose the output scale system from: mm, cm, dm, m, km");

        //USER SELECTS OUTPUT UNIT TYPE

        System.out.println("Please select output unit (mm/cm/dm/m/km)");
        scale = scanner.nextLine();

        Calculator calculator = new Calculator(scale);
        System.out.println(calculator.calculate(expression));




    }
}