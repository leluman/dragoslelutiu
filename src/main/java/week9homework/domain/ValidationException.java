package week9homework.domain;

public class ValidationException extends RuntimeException{
    public ValidationException(String message) {
        super(message);
    }
}
