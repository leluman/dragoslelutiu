package week9homework.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class StudentRepository {

    List<Student> students = new ArrayList<>();
    Logger logger = LoggerFactory.getLogger(StudentRepository.class);

    public void add(Student student) {
        this.students.add(student);
    }

    public void deleteByCNP(String CNP) {

        if (CNP == null) {
            logger.error("CNP is empty.");
            throw new ValidationException("CNP is empty. Please add valid CNP.");
        }

        boolean found = false;
        for (Student student : students) {
            if (student.getCNP().equals(CNP)) {
                students.remove(student);
                found = true;
                break;
            }
        }

        if (!found) {
            logger.error("Student does not exist.");
            throw new ValidationException("Student does not exist.");
        }
    }

    public List<Student> retrieveStudents(String age) {
        logger.trace("Retrieving students");

        int value;
        try {
            value = Integer.parseInt(age);
        } catch (NumberFormatException exception) {
            throw new ValidationException("Age is not a number.");
        }

        if (value < 0) {
            throw new ValidationException("Age is negative.");
        }

        List<Student> studentsByAge = new ArrayList<>();
        int calculatedAge;
        for (Student student : students) {
            calculatedAge = LocalDate.now().getYear() - student.getBirthDate().getYear();
            if (calculatedAge == value) {
                studentsByAge.add(student);
            }
        }
        return studentsByAge;
    }

    public void listStudents() {
        for (Student student : students) {
            if (student == null) {
                throw new ValidationException("Input empty.");
            }
        }
        this.students.sort(Comparator.comparingInt(o -> o.getBirthDate().getYear()));
        System.out.println(students);

    }
}
