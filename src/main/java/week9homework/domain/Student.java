package week9homework.domain;

import java.time.LocalDate;

public class Student {

    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private Gender gender;
    private String CNP;

    public Student(String firstName, String lastName, Gender gender, LocalDate birthDate, String CNP) {

        if (firstName != null && lastName != null) {
            this.firstName = firstName;
            this.lastName = lastName;
        } else {
            throw new ValidationException("First name and last name must not be empty. Please fill in both fields");
        }
        this.gender = gender;

        int birthYear = birthDate.getYear();
        if(birthYear < 1900) {
            throw new ValidationException("Birthdate must be 1900 or above.");
        } else if (birthYear > LocalDate.now().getYear() - 18) {
            throw new ValidationException("All students must be 18 or older");
        } else {
            this.birthDate = birthDate;
        }

        if (CNP != null && CNP.length() == 13) {
            this.CNP = CNP;
        } else {
            throw new ValidationException("Wrong CNP. Please try again");
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender=" + gender +
                ", birthDate=" + birthDate +
                ", CNP='" + CNP + '\'' +
                '}';
    }
}
