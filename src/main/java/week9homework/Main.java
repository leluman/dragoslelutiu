package week9homework;

import week9homework.domain.Gender;
import week9homework.domain.Student;
import week9homework.domain.StudentRepository;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {

        StudentRepository studentRepository = new StudentRepository();

        Student student1 = new Student("Dragos", "Lelutiu", Gender.convert("m"), LocalDate.of(1988,4,26),"1880426008089");
        Student student2 = new Student("Alin", "Cozma", Gender.convert("m"), LocalDate.of(1956,5,13),"1560513008089");
        Student student3 = new Student("Andrei", "Mititelu", Gender.convert("m"), LocalDate.of(2003,6,10),"1030610008089");
        Student student4 = new Student("Andreea", "Bilbor", Gender.convert("f"), LocalDate.of(1988,7,17),"1880717008089");
        Student student5 = new Student("Larisa", "Negrusi", Gender.convert("f"), LocalDate.of(1975,8,20),"1750820008089");
        Student student6 = new Student("Matei", "Stanulet", Gender.convert("m"), LocalDate.of(1945,6,3),"1450603008089");
        Student student7 = new Student("Alex", "Colta", Gender.convert("m"), LocalDate.of(2002,5,15),"1080515008089");
        Student student8 = new Student("Alexandra", "Dobre", Gender.convert("f"), LocalDate.of(1978,2,7),"1780207008089");
        Student student9 = new Student("Ioana", "Lupascu", Gender.convert("f"), LocalDate.of(1990,3,31),"1900310080891");
        Student student10 = new Student("Ionela", "Ureche", Gender.convert("f"), LocalDate.of(1986,1,5),"1860105008089");

        studentRepository.add(student1);
        studentRepository.add(student2);
        studentRepository.add(student3);
        studentRepository.add(student4);
        studentRepository.add(student5);
        studentRepository.add(student6);
        studentRepository.add(student7);
        studentRepository.add(student8);
        studentRepository.add(student9);
        studentRepository.add(student10);

        studentRepository.listStudents();

        studentRepository.deleteByCNP("1880426008089"); //delete Dragos;
        studentRepository.listStudents();

        System.out.println(studentRepository.retrieveStudents("20"));

    }
}
