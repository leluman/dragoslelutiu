package week8homework.domain;

import com.opencsv.bean.CsvBindByName;

import java.time.Duration;

public class Athlete {

    @CsvBindByName
    private String athleteNumber;

    @CsvBindByName
    private String AthleteName;

    @CsvBindByName
    private String CountryCode;

    @CsvBindByName
    private String SkiTimeResult;

    @CsvBindByName
    private String FirstShootingRange;

    @CsvBindByName
    private String SecondShooting;

    @CsvBindByName
    private String ThirdShooting;

    private Integer timeResult;

    private Integer timePenalty;

    private Integer finalTimeResult;

    public Integer getTimeResult() { return timeResult; }

    public void setTimeResult(int timeResult) { this.timeResult = timeResult; }

    public Integer getTimePenalty() { return timePenalty; }

    public void setTimePenalty(int timePenalty) { this.timePenalty = timePenalty; }

    public Integer getFinalTimeResult() { return finalTimeResult; }

    public void setFinalTimeResult(int finalTimeResult) { this.finalTimeResult = finalTimeResult; }

    public String getAthleteNumber() {
        return athleteNumber;
    }

    public void setAthleteNumber(String athleteNumber) {
        this.athleteNumber = athleteNumber;
    }

    public String getAthleteName() {
        return AthleteName;
    }

    public void setAthleteName(String athleteName) {
        AthleteName = athleteName;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getSkiTimeResult() { return SkiTimeResult; }

    public void setSkiTimeResult(String skiTimeResult) {
        SkiTimeResult = skiTimeResult;
    }

    public String getFirstShootingRange() {
        return FirstShootingRange;
    }

    public void setFirstShootingRange(String firstShootingRange) {
        FirstShootingRange = firstShootingRange;
    }

    public String getSecondShooting() {
        return SecondShooting;
    }

    public void setSecondShooting(String secondShooting) {
        SecondShooting = secondShooting;
    }

    public String getThirdShooting() {
        return ThirdShooting;
    }

    public void setThirdShooting(String thirdShooting) {
        ThirdShooting = thirdShooting;
    }

    @Override
    public String toString() {
        return "Athlete{" +
                "athleteNumber='" + athleteNumber + '\'' +
                ", AthleteName='" + AthleteName + '\'' +
                ", CountryCode='" + CountryCode + '\'' +
                ", SkiTimeResult=" + SkiTimeResult +
                ", FirstShootingRange='" + FirstShootingRange + '\'' +
                ", SecondShooting='" + SecondShooting + '\'' +
                ", ThirdShooting='" + ThirdShooting + '\'' +
                '}';
    }
}
