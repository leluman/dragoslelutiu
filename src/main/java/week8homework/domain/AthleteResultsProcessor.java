package week8homework.domain;

import com.opencsv.bean.CsvToBeanBuilder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AthleteResultsProcessor {


    public static List<Athlete> getListfromCSVfile (String csvName) {
        List<Athlete> copyBeans = null;

        try {
            List<Athlete> beans = new CsvToBeanBuilder(new FileReader(csvName))
                    .withType(Athlete.class).build().parse();
            copyBeans = beans;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return copyBeans;

    }

    public static List<ShootingResult> convertToShootingResults(String results) {
        List<ShootingResult> shootingResults = new ArrayList<>();
        for (int i = 0; i < results.length(); i++) {
            char c = results.charAt(i);
            shootingResults.add(ShootingResult.convert(c));
        }
        return shootingResults;
    }

    public static int getPenaltySeconds(String shootingResult) {
        List<ShootingResult> ShootingResultsList = AthleteResultsProcessor.convertToShootingResults(shootingResult);
        int penaltySeconds = 0;
        for (ShootingResult a : ShootingResultsList ) {
            if (a == ShootingResult.MISS) {
                penaltySeconds = penaltySeconds + 10;
            }
        }
        return penaltySeconds;
    }

    public static int fromStringTimeToIntTime(String s) {
        int result = 0;
        String[] time = s.split(":");
        result = Integer.parseInt(time[0]) * 60 + Integer.parseInt(time[1]);
        return result;

        }



}
