package week8homework.domain;

import java.util.Comparator;


public class AthleteComparator implements Comparator<Athlete> {

    @Override
    public int compare(Athlete a1, Athlete a2) {
        return a1.getFinalTimeResult().compareTo(a2.getFinalTimeResult());
    }


}
