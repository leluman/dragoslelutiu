package week8homework;



import com.opencsv.bean.CsvToBeanBuilder;
import week8homework.domain.Athlete;
import week8homework.domain.AthleteComparator;
import week8homework.domain.AthleteResultsProcessor;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;


public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Hello world!");
        List<Athlete> copyBeans = null;

        // Reading the information from the file in a list
        copyBeans = AthleteResultsProcessor.getListfromCSVfile("D:\\Dragos\\Curs_JAVA_Development\\DragosLelutiu\\src\\main\\resources\\ski_results_2.csv");


        // Iterating through the list to print the information before sorting
        for (Athlete at: copyBeans) {
            System.out.println(at.getAthleteNumber() + "  " + at.getAthleteName() + "  " + at.getCountryCode() +
                    "  " + at.getSkiTimeResult() + "  " + at.getFirstShootingRange() + "  " +
                    at.getSecondShooting() + "  " + at.getThirdShooting() + " " + at.getTimeResult() + " " +
                    at.getTimePenalty() + " " + at.getFinalTimeResult());
        }

        for (Athlete at : copyBeans) {
            at.setTimeResult(AthleteResultsProcessor.fromStringTimeToIntTime(at.getSkiTimeResult()));

            at.setTimePenalty(AthleteResultsProcessor.getPenaltySeconds(at.getFirstShootingRange()) +
                    AthleteResultsProcessor.getPenaltySeconds(at.getSecondShooting()) +
                    AthleteResultsProcessor.getPenaltySeconds(at.getThirdShooting()));

            at.setFinalTimeResult(AthleteResultsProcessor.fromStringTimeToIntTime(at.getSkiTimeResult())+ AthleteResultsProcessor.getPenaltySeconds(at.getFirstShootingRange()) +
                    AthleteResultsProcessor.getPenaltySeconds(at.getSecondShooting()) +
                    AthleteResultsProcessor.getPenaltySeconds(at.getThirdShooting()));

            System.out.println(at.getAthleteNumber() + "  " + at.getAthleteName() + "  " + at.getCountryCode() +
                    "  " + at.getSkiTimeResult() + "  " + at.getFirstShootingRange() + "  " +
                    at.getSecondShooting() + "  " + at.getThirdShooting() + " " + at.getTimeResult() + " " +
                    at.getTimePenalty() + " " + at.getFinalTimeResult());
        }

        // Sorting the list of athletes based on final time result
        copyBeans.sort(new AthleteComparator());
        System.out.println();

        System.out.println("RESULTS");

        System.out.println("Winner - " + copyBeans.get(0).getAthleteName() + " " + copyBeans.get(0).getSkiTimeResult()
                        + " (" + copyBeans.get(0).getSkiTimeResult() + " + " + copyBeans.get(0).getTimePenalty() + ")" );

        System.out.println("RUnner-up - " + copyBeans.get(1).getAthleteName() + " " + copyBeans.get(1).getSkiTimeResult()
                + " (" + copyBeans.get(1).getSkiTimeResult() + " + " + copyBeans.get(1).getTimePenalty() + ")" );

        System.out.println("Third place - " + copyBeans.get(2).getAthleteName() + " " + copyBeans.get(2).getSkiTimeResult()
                + " (" + copyBeans.get(2).getSkiTimeResult() + " + " + copyBeans.get(2).getTimePenalty() + ")" );






    }



}
