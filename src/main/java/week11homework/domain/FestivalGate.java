package week11homework.domain;

import java.util.ArrayDeque;
import java.util.Queue;

public class FestivalGate {

    private Queue<TicketType> queue = new ArrayDeque<>();

    public Queue<TicketType> getQueue() {
        return ((ArrayDeque<TicketType>)queue).clone();
    }

    public boolean validateTicket (TicketType ticketType){
        queue.add(ticketType);
        return true;
    }
}
