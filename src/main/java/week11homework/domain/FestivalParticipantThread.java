package week11homework.domain;

import java.util.Random;

public class FestivalParticipantThread extends Thread{

    private final TicketType ticketType;
    private final FestivalGate gate;

    public FestivalParticipantThread(TicketType ticketType, FestivalGate gate) {
        this.ticketType = ticketType;
        this.gate = gate;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(new Random().nextInt(500, 4000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        gate.validateTicket(ticketType);
        System.out.println("Thread with ID " + this.getId() + " started");
    }
}
