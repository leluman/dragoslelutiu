package week11homework;

import week11homework.domain.FestivalGate;
import week11homework.domain.FestivalParticipantThread;
import week11homework.domain.FestivalStatisticsThread;
import week11homework.domain.TicketType;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {

    private static final List<TicketType> VALUES = Collections.unmodifiableList(Arrays.asList(TicketType.values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    // Return a random ticket type
    public static TicketType randomTicket() {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }

    public static void main(String[] args) {

        FestivalGate festivalGate = new FestivalGate();
        FestivalStatisticsThread statisticsThread = new FestivalStatisticsThread(festivalGate);
        statisticsThread.start();

        for (int i = 0; i < 150; i++) {
            FestivalParticipantThread attendeeThread = new FestivalParticipantThread(randomTicket(), festivalGate);
            attendeeThread.start();
        }
    }
}
