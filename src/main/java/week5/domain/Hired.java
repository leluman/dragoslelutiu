package week5.domain;

public class Hired extends Person{

    public Hired(String name, int age, String CNP) {
        super(name, age, CNP);
    }

    @Override
    public String toString() {
        return "Hired{" +
                super.toString();
    }
}
