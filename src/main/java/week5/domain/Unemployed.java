package week5.domain;

public class Unemployed extends Person{

    public Unemployed(String name, int age, String CNP) {
        super(name, age, CNP);
    }

    @Override
    public String toString() {
        return "Unemployed{" +
                super.toString();
    }
}
