package week5.domain;

import java.util.Comparator;

public class PersonSortingComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Person person1 = (Person) o1;
        Person person2 = (Person) o2;

        int NameCompare = person1.getName().compareTo(person2.getName());

        int AgeCompare = Integer.compare(person1.getAge(), person2.getAge());

        // 2nd level comparison
        return (NameCompare == 0) ? AgeCompare
                : NameCompare;

    }
}
