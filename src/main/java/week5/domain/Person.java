package week5.domain;

import java.util.Objects;

public class Person {

    private String name;
    private int age;
    private String CNP;


    public Person(String name, int age, String CNP) {
        this.name = name;
        this.age = age;
        this.CNP = CNP;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", CNP='" + CNP + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null) return false;
        final Person person = (Person) o;
        return CNP.equals(person.CNP);
    }

    @Override
    public int hashCode() {
        return Objects.hash(CNP);
    }



}
