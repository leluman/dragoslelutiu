package week5.domain;

public class Student extends Person {

    public Student(String name, int age, String CNP) {
        super(name, age, CNP);
    }

    @Override
    public String toString() {
        return "Student{" +
                super.toString();
    }
}
