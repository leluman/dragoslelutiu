package week5;

import week5.domain.*;


import java.util.*;

public class Main {

    public static void main(String[] args) {

        // TreeSet part of homework

        // Creating the persons
        Person person1 = new Student("Catalin",18, "123467894");
        Person person2 = new Hired("Cosmin", 16, "122345687");
        Person person3 = new Unemployed("Madalina", 25, "212234568");
        Person person4 = new Student("Mihai", 40, "321234568");
        Person person5 = new Student("Alin",19, "987654321");
        Person person6 = new Hired("Alina", 12, "122322356");
        Person person7 = new Unemployed("Daniel", 55,"456789123");
        Person person8 = new Hired("Daniel", 14,"432978153");
        Person person9 = new Student("Alexandru",33,"153624759");
        Person person10 = new Student("Iulian",10,"759486123");
        Person person11 = new Student("Iulica",15,"183729456");
        Person person12 = new Hired("Alexandra",8,"146365486");


        // Creation of TreeSet and adding the persons to TreeSet
        // The persons are added in alphabetical and age ascending order

        Set<Person> sortedPersons = new TreeSet<>(new PersonSortingComparator());
        sortedPersons.add(person1);
        sortedPersons.add(person2);
        sortedPersons.add(person3);
        sortedPersons.add(person4);
        sortedPersons.add(person5);
        sortedPersons.add(person6);
        sortedPersons.add(person7);
        sortedPersons.add(person8);
        sortedPersons.add(person9);
        sortedPersons.add(person10);
        sortedPersons.add(person11);
        sortedPersons.add(person12);

        //Iteration through the TreeSet printing name and age

        for(Person e:sortedPersons) {
            System.out.println("Name: " + e.getName() + " Age: " + e.getAge());
        }


        // HashMap part of homework


        // Creating addresses for hobbies

        Address address1 = new Address("Romania","Brasov","Bobalna Street",15);
        Address address2 = new Address("USA","Torrance","Linda Drive",22126);
        Address address3 = new Address("Romania","Brasov","Carierei Street",3);
        Address address4 = new Address("Germany","Koln","Weinsbergstraße",118);
        Address address5 = new Address("France","Paris","Champs-Élysées",138);
        Address address6 = new Address("Italy","Bologna","Piazza Galvani",5);
        Address address7 = new Address("Austria","Vienna","Stephansplatz",3);
        Address address8 = new Address("Spain","Barcelona","Calle de Mallorca",401);

        // Creating lists of addresses for each hobby

        List<Address> addressesForCycling = new ArrayList<>();
        addressesForCycling.add(address1);
        addressesForCycling.add(address2);

        List<Address> addressesForSwimming = new ArrayList<>();
        addressesForSwimming.add(address3);
        addressesForSwimming.add(address4);

        List<Address> addressesForSnowboarding = new ArrayList<>();
        addressesForSnowboarding.add(address5);
        addressesForSnowboarding.add(address6);
        addressesForSnowboarding.add(address7);

        List<Address> addressesForDancing = new ArrayList<>();
        addressesForDancing.add(address8);
        addressesForDancing.add(address2);

        List<Address> addressesForPainting = new ArrayList<>();
        addressesForPainting.add(address5);
        addressesForPainting.add(address2);

        List<Address> addressesForGym = new ArrayList<>();
        addressesForGym.add(address3);
        addressesForGym.add(address4);


        // Creating hobbies

        Hobby hobby1 = new Hobby("cycling",3, addressesForCycling);
        Hobby hobby2 = new Hobby("swimming",4, addressesForSwimming);
        Hobby hobby3 = new Hobby("snowboarding",3, addressesForSnowboarding);
        Hobby hobby4 = new Hobby("dancing",4, addressesForDancing);
        Hobby hobby5 = new Hobby("painting",3, addressesForPainting);
        Hobby hobby6 = new Hobby("gym",4, addressesForGym);


        // Creating list of hobbies for teenagers (age 13 to 19)
        List<Hobby> hobbiesForTeenagers = new ArrayList<>();
        hobbiesForTeenagers.add(hobby3);
        hobbiesForTeenagers.add(hobby2);
        hobbiesForTeenagers.add(hobby1);

        // Creating list of hobbies for adults (age over 20)
        List<Hobby> hobbiesForAdults = new ArrayList<>();
        hobbiesForAdults.add(hobby4);
        hobbiesForAdults.add(hobby6);
        hobbiesForAdults.add(hobby1);
        hobbiesForAdults.add(hobby3);

        // Creating list of hobbies for kids (age under 13)
        List<Hobby> hobbiesForKids = new ArrayList<>();
        hobbiesForKids.add(hobby5);
        hobbiesForKids.add(hobby4);
        hobbiesForKids.add(hobby2);

        // Creating HashMap

        Map<Person, List<Hobby>> personToHobbies = new HashMap<>();
        personToHobbies.put(person1, hobbiesForTeenagers);
        personToHobbies.put(person2, hobbiesForTeenagers);
        personToHobbies.put(person3, hobbiesForAdults);
        personToHobbies.put(person4, hobbiesForAdults);
        personToHobbies.put(person5, hobbiesForAdults);
        personToHobbies.put(person6, hobbiesForKids);
        personToHobbies.put(person7, hobbiesForAdults);
        personToHobbies.put(person8, hobbiesForTeenagers);
        personToHobbies.put(person9, hobbiesForAdults);
        personToHobbies.put(person10, hobbiesForKids);
        personToHobbies.put(person11, hobbiesForTeenagers);
        personToHobbies.put(person12, hobbiesForKids);


        // Print for a certain Person, the names of the hobbies and the countries where it can be practiced
        // Showing results for person3

        System.out.println("\nShowing results for: " + person3.getName());
        for (Hobby hobby : personToHobbies.get(person3)) {
            System.out.print("\nHobby: " + hobby.getName() + "\nCountries:");
            for (Address address : hobby.getAddresses()) {
                System.out.print(address.getCountry() + ";");
            }
            System.out.println();
        }


    }
}
