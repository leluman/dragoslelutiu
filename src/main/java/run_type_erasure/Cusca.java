package run_type_erasure;

public class Cusca <T extends Animal>{

    private T animal;

    public Cusca(T animal) {

        this.animal = animal;
    }


    public T getAnimal() {
        return animal;
    }
}
