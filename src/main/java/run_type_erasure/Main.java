package run_type_erasure;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Dog dog = new Dog();
        Cat cat = new Cat();
        Animal animal = new Dog();
        Object animal2 = new Cat();

        Cusca<Cat> cage = new Cusca(dog);
//        cage.getAnimal().bark(); // contraexemplu
        Cusca<Dog> cage1 = new Cusca(dog);
        cage1.getAnimal().bark();
        Cusca<Animal> cage2 = new Cusca(cat);
        Cusca cusca3 = new Cusca(animal);
        Cusca cusca4 = new Cusca((Animal)animal2);
        Cusca cusca5 = new Cusca((Cat)animal2);
//        Cusca cusca6 = new Cusca(new Object());

        List<Animal> list = new ArrayList<>(); //this is what it looks at compile time
        List<Object> list2 = new ArrayList<>(); // this is how it looks at runtime
//        @SuppressWarnings({"unused"})
        @SuppressWarnings({"unchecked"})
        List lista3 = new ArrayList<>();

        lista3.add(cage);

        list.add(dog);
        list.add(cat);
        list.add(animal);

        for (Animal a: list) {
            a.getID();
        }

        List<NonSellable> list3 = new ArrayList<>();
        list3.add(cat);

//        if (cat instanceof NonSellable)

//        if (cat.getClass().getTypeName().equals(NonSellable.class.toString()))

        Runnable a = new Runnable() {
            @Override
            public void run() {

            }
        };

        Runnable b = () -> {};




    }
}
