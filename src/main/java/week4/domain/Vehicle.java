package week4.domain;

public abstract class Vehicle implements VehicleBehaviour {

    public int fuelTankSize;
    public String fuelType;
    public int gear;
    public int gears;
    public double consumptionPer100Km;
    public float availableFuel;
    public int tireSize;
    public String VIN;
    public String model;
    public String brand;
    public Boolean isOn;
    public double currentConsumption;
    public int currentDistance;
    public double averageConsumption;
    public double varConsumptionPer100Km;

    public Vehicle(int fuelTankSize, String fuelType, int gears, float availableFuel, int tireSize, String VIN, double consumptionPer100Km) {
        this.fuelTankSize = fuelTankSize;
        this.fuelType = fuelType;
        this.gears = gears;
        this.availableFuel = availableFuel;
        this.tireSize = tireSize;
        this.VIN = VIN;
        this.consumptionPer100Km = consumptionPer100Km;
        this.isOn = false;
    }

    @Override
    public void start() {
        if (!isOn) {
            isOn = true;
            currentConsumption = 0;
            currentDistance = 0;
            averageConsumption = 0;
            varConsumptionPer100Km = 0;
        }
        else {
            System.out.println("The engine is already turned on!");
        }
    }

    @Override
    public void stop() {
        if (isOn == true) {
            averageConsumption = currentConsumption / currentDistance;
            System.out.println("Consumul mediu a fost:" + currentConsumption);
            isOn = false;
        }
        else {
            System.out.println("The engine is already off!");
        }
    }

    @Override
    public void move(int distance) {

        currentDistance = currentDistance + distance;
        currentConsumption = currentConsumption + (distance * varConsumptionPer100Km)/100;

    }

    @Override
    public void shiftGear(int gear) {

        switch (gear) {
            case 1:
                varConsumptionPer100Km = consumptionPer100Km;
                System.out.println("consumul in viteza intai este: " + consumptionPer100Km);
                break;
            case 2:
                varConsumptionPer100Km = varConsumptionPer100Km - (2 * consumptionPer100Km)/100;
                System.out.println("consumul in viteza a doua este: " + varConsumptionPer100Km);
                break;
            case 3:
                varConsumptionPer100Km = varConsumptionPer100Km - (3 * consumptionPer100Km)/100;
                System.out.println("consumul in viteza a treia este: " + varConsumptionPer100Km);
                break;
            case 4:
                varConsumptionPer100Km = varConsumptionPer100Km - (4 * consumptionPer100Km)/100;
                System.out.println("consumul in viteza a patra este: " + varConsumptionPer100Km);
                break;
            case 5:
                varConsumptionPer100Km = varConsumptionPer100Km - (5 * consumptionPer100Km)/100;
                System.out.println("consumul in viteza a cincea este: " + varConsumptionPer100Km);
                break;
            case 6:
                varConsumptionPer100Km = varConsumptionPer100Km - (6 * consumptionPer100Km)/100;
                System.out.println("consumul in viteza a sasea este: " + varConsumptionPer100Km);
                break;

        }
    }
}
