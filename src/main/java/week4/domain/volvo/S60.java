package week4.domain.volvo;

public class S60 extends Volvo{

    public S60(int fuelTankSize, String fuelType, int gears, float availableFuel, int tireSize, String VIN, float consumptionPer100Km) {
        super(fuelTankSize, fuelType, gears, availableFuel, tireSize, VIN, consumptionPer100Km);
        this.fuelTankSize = fuelTankSize;
        this.fuelType = fuelType;
        this.gears = gears;
        this.availableFuel = availableFuel;
        this.tireSize = tireSize;
        this.VIN = VIN;
        this.consumptionPer100Km = consumptionPer100Km;
        this.model = "S60";
    }

    @Override
    public String toString() {
        return "S60{" +
                "fuelTankSize=" + fuelTankSize +
                ", fuelType='" + fuelType + '\'' +
                ", gears=" + gears +
                ", consumptionPer100Km=" + consumptionPer100Km +
                ", availableFuel=" + availableFuel +
                ", tireSize=" + tireSize +
                ", VIN='" + VIN + '\'' +
                ", model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }
}
