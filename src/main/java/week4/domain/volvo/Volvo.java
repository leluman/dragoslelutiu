package week4.domain.volvo;

import week4.domain.Vehicle;

public abstract class Volvo extends Vehicle {

    public Volvo(int fuelTankSize, String fuelType, int gears, float availableFuel, int tireSize, String VIN, float consumptionPer100Km) {
        super(fuelTankSize, fuelType, gears, availableFuel,tireSize, VIN, consumptionPer100Km);
        this.brand = "Volvo";
    }

    @Override
    public String toString() {
        return "Volvo{" +
                "fuelTankSize=" + fuelTankSize +
                ", fuelType='" + fuelType + '\'' +
                ", gears=" + gears +
                ", consumptionPer100Km=" + consumptionPer100Km +
                ", availableFuel=" + availableFuel +
                ", tireSize=" + tireSize +
                ", VIN='" + VIN + '\'' +
                ", model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }
}
