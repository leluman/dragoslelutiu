package week4.domain;

public interface VehicleBehaviour {

    void start();
    void stop();
    void move(int distance);
    void shiftGear(int gear);

}
