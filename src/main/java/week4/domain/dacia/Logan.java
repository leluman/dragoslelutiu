package week4.domain.dacia;

public class Logan extends Dacia{

    public Logan(int fuelTankSize, String fuelType, int gears, float availableFuel, int tireSize, String VIN, double consumptionPer100Km) {
        super(fuelTankSize, fuelType, gears, availableFuel, tireSize, VIN, consumptionPer100Km);
        this.fuelTankSize = fuelTankSize;
        this.fuelType = fuelType;
        this.gears = gears;
        this.availableFuel = availableFuel;
        this.tireSize = tireSize;
        this.VIN = VIN;
        this.consumptionPer100Km = consumptionPer100Km;
        this.model = "Logan";
    }

    @Override
    public String toString() {
        return "Logan{" +
                "fuelTankSize=" + fuelTankSize +
                ", fuelType='" + fuelType + '\'' +
                ", gears=" + gears +
                ", consumptionPer100Km=" + consumptionPer100Km +
                ", availableFuel=" + availableFuel +
                ", tireSize=" + tireSize +
                ", VIN='" + VIN + '\'' +
                ", model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }
}
