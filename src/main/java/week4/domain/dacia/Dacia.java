package week4.domain.dacia;

import week4.domain.Vehicle;

public abstract class Dacia extends Vehicle {

    public Dacia(int fuelTankSize, String fuelType, int gears, float availableFuel, int tireSize, String VIN, double consumptionPer100Km) {
        super(fuelTankSize, fuelType, gears, availableFuel,tireSize, VIN, consumptionPer100Km);
        this.brand = "Dacia";
    }

    @Override
    public String toString() {
        return "Dacia{" +
                "fuelTankSize=" + fuelTankSize +
                ", fuelType='" + fuelType + '\'' +
                ", gears=" + gears +
                ", consumptionPer100Km=" + consumptionPer100Km +
                ", availableFuel=" + availableFuel +
                ", tireSize=" + tireSize +
                ", VIN='" + VIN + '\'' +
                ", model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }
}
