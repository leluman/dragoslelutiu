package week4;

import week4.domain.Vehicle;
import week4.domain.dacia.Logan;
import week4.domain.dacia.Sandero;

public class Main {

    public static void main(String[] args) {

        Vehicle car1 = new Logan(50, "DIESEL", 6, 45, 15, "dacialogan1212", 4.5);
        Vehicle car2 = new Sandero(40, "GASOLINE", 5, 30, 13, "daciasandero1212", 6);

        car1.start();
        car1.shiftGear(1);
        car1.move(50);
        car1.shiftGear(4);
        car1.move(150);
        car1.shiftGear(3);
        car1.move(20);
        car1.shiftGear(2);
        car1.move(20);
        car1.shiftGear(1);
        car1.move(50);
        car1.start();
        car1.stop();
        car1.stop();


    }
}
