package week10homework;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;



class MainTest {

    @Test
    void filterByMonth_success() {
        String line = "Lelutiu,Dragos,1988.04.26";
        assertTrue(Main.filterByMonth(line,"04"));
    }

    @Test
    void findMonth_success() {
        String date = "2007.05.26";
        assertEquals("05",Main.findMonth.apply(date));
    }

    @Test
    void processStream_success() {

        Stream<String> stream = Stream.of("Lelutiu,Dragos,1988.04.26","Precup,Adrian,2002.12.13","Adam,Catalin,1988.04.22");
        List<String> list = Main.processStream(stream,"04");

        assertEquals("Adam,Catalin,1988.04.22",list.get(0));
        assertEquals("Lelutiu,Dragos,1988.04.26",list.get(1));
        assertEquals(2,list.size());
    }
}