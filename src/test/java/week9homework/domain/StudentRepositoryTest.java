package week9homework.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class StudentRepositoryTest {

    StudentRepository studentRepository;
    Student student;

    @BeforeEach
    public void init() {
        studentRepository = new StudentRepository();
        student = new Student("Dragos", "Lelutiu", Gender.convert("m"), LocalDate.of(1988,4,26),"1880426008089");
    }

    @Test
    void add_success() {
        studentRepository.add(student);
        assertEquals(1, studentRepository.students.size());
    }

    @Test
    void deleteByCNP_CNPisNull_exception() {
        Exception e = assertThrows(ValidationException.class, () -> studentRepository.deleteByCNP(null));
        assertEquals("CNP is empty. Please add valid CNP.", e.getMessage());
    }

    @Test
    void deleteByCNP_studentNotFound_Exception() {
        studentRepository.add(student);
        Exception e = assertThrows(ValidationException.class, () -> studentRepository.deleteByCNP("1880426008082"));
        assertEquals("Student does not exist.", e.getMessage());
    }

    @Test
    void deleteByCNP_success() {
        studentRepository.add(student);
        studentRepository.deleteByCNP("1880426008089");
        assertEquals(0, studentRepository.students.size());
    }

    @Test
    void retrieveStudents_ageNotANumber_exception() {
        Exception e = assertThrows(ValidationException.class, () -> studentRepository.retrieveStudents("a1"));
        assertEquals("Age is not a number.", e.getMessage());
    }

    @Test
    void retrieveStudents_ageIsNegative_exception() {
        Exception e = assertThrows(ValidationException.class, () -> studentRepository.retrieveStudents("-2"));
        assertEquals("Age is negative.", e.getMessage());
    }

    @Test
    void retrieveStudents_success() {
        studentRepository.add(student);
        assertEquals(1, studentRepository.retrieveStudents("34").size());
    }



}