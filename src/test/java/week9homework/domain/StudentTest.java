package week9homework.domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    @Test
    public void createStudent_emptyFirstName_exception() {
        Exception e = assertThrows(ValidationException.class,
                () -> new Student(null, "A", Gender.MALE, LocalDate.now(), "1234"));
        assertEquals("First name and last name must not be empty. Please fill in both fields", e.getMessage());
    }

    @Test
    public void createStudent_youngerThan18_exception() {
        Exception e = assertThrows(ValidationException.class, () ->
                new Student("Dragos", "Lelutiu", Gender.MALE, LocalDate.of(2006, 1, 1), "1234"));
        assertEquals("All students must be 18 or older", e.getMessage());
    }

    @Test
    public void createStudent_bornBefore1900_exception() {
        Exception e = assertThrows(ValidationException.class, () ->
                new Student("Biljana", "Apetrei", Gender.FEMALE, LocalDate.of(1820, 4, 13), "1234"));
        assertEquals("Birthdate must be 1900 or above.", e.getMessage());
    }

    @Test
    public void createStudent_nullCNP_exception() {
        Exception e = assertThrows(ValidationException.class, () ->
                new Student("Ioana", "Lupascu", Gender.FEMALE, LocalDate.of(1990, 11, 14), null));
        assertEquals("Wrong CNP. Please try again", e.getMessage());
    }

    @Test
    public void createStudent_wrongCNP_exception() {
        Exception e = assertThrows(ValidationException.class, () ->
                new Student("Bogdan", "Amariei", Gender.MALE, LocalDate.of(1987, 12, 11), "1234"));
        assertEquals("Wrong CNP. Please try again", e.getMessage());
    }


}