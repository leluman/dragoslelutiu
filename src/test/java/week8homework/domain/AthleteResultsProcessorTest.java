package week8homework.domain;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AthleteResultsProcessorTest {

    @Test
    void convertToShootingResults_success() {

        List<ShootingResult> shootingResults = AthleteResultsProcessor.convertToShootingResults("xxoox");
        assertEquals(5,shootingResults.size());
        assertEquals(ShootingResult.HIT, shootingResults.get(0));
        assertEquals(ShootingResult.HIT, shootingResults.get(1));
        assertEquals(ShootingResult.MISS, shootingResults.get(2));
        assertEquals(ShootingResult.MISS, shootingResults.get(3));
        assertEquals(ShootingResult.HIT, shootingResults.get(4));
    }

    @Test
    void getPenaltySeconds_success() {
        int penaltySeconds = AthleteResultsProcessor.getPenaltySeconds("xxoox");
        assertEquals(20,penaltySeconds);
    }

}