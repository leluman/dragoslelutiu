package week6.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private Calculator calculator;
    private String expression, scale;
    private double value;

    @BeforeEach
    void setUp() {
        scale = "mm";
        calculator = new Calculator(scale);
        expression = "10 cm + 1 m - 10 mm";

    }

    @Test
    void calculateExpression_success() {
        assertEquals(1090, calculator.calculate(expression));

    }

    @Test
    void displayScale_success() {
        String scale = "cm";
        calculator = new Calculator(scale);
        assertEquals(109.0, calculator.calculate(expression));
    }
}